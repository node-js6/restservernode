// ==============
// PORT
// ==============
process.env.PORT = process.env.PORT || 3000;
// ==============
// ENVIROMENT
// ==============
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';
// ==============
// DATABASE
// ==============

let urlDB;

if ( process.env.NODE_ENV === 'dev' ){
    urlDB = 'mongodb://localhost:27017/cafe';
}else{
     urlDB = process.env.MONGO_URI;
 }
// ==============
// JWT
// ==============

//60 segundos
//60 minutos
//24 horas
//30 dias

process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30;

//seed semilla

process.env.SEED =  process.env.SECRET_JWT || 'my-secret';






process.env.URL_DB = urlDB;
