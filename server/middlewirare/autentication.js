const jwt = require('jsonwebtoken');


//===========================
//=======VERIFICAR_TOKEN=====

let verificaToken = (req, res, next) => {
    let token = req.get('token');

    jwt.verify(token, process.env.SEED,( err, decoded ) =>{

        if (err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        req.usuario = decoded.usuario;
        next();
    });



};

//=================================
//=======VERIFICAR_GRANT_ADMIN=====

let verifyGranTUser = (req, res, next) => {
    let rol = req.rol;

        if(rol === 'ADMIN_ROL'){
            next();
            return
        }

        return res.status(500).json({
            ok: false,
            message: 'El usuario no tiene permisos para realizar esta transaccion.'
    });
        


}


module.exports = {
    verificaToken,
    verifyGranTUser
};