const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;


let rolesValidos = {
    values: ['USER_ROL','ADMIN_ROL'],
    message: '{VALUE} no es un rol valido'
};

let userSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        unique: true,
        required : [true, 'el email es obligatorio']
    },
    password: {
        type: String,
        required: [true, 'el password es obligatorio']
    },
    img:{
        type: String,
        required: false
    },
    rol: {
        type: String,
        default: 'USER_ROL',
        enum: rolesValidos
    },
    state: {
        type: Boolean,
        default: true
    },
    google: {
        type: Boolean,
        default: false
    }

});

userSchema.methods.toJSON = function(){
    let user = this;
    let userObject = user.toObject();
    delete userObject.password;
    return userObject;
}

userSchema.plugin(uniqueValidator, {
    message: '{PATH} debe de ser unico'
});
module.exports = mongoose.model( 'Usuario', userSchema );