
const fs = require('fs');
const path = require('path');
const express = require('express');
const fileUpload = require('express-fileupload');

const app = express();


app.get('/imagen/:tipo/:img/', (req, res) => {
    let tipo = req.params.tipo
    let img = req.params.img;
    
    let noImageFind = path.resolve(__dirname , `../assets/img/original.jpg`);
    let pathImage = path.resolve( __dirname, `../../uploads/${tipo}/${img}` )
    
    if (fs.existsSync(pathImage)){
        res.sendFile(pathImage);
    }else{
        res.sendFile(noImageFind);
    }
    
    
});

module.exports = app;