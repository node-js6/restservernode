const express = require('express')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const _  = require('underscore');
const bodyParser = require('body-parser')
const Usuario = require('../models/usuario')

const app = express();

app.post('/login', (req,res) => {

    let body = req.body;

    Usuario.findOne({email: body.email}, (err, usuarioDB)=> {
        if (err) {
            return res.status(500).json({
               ok: false,
               err     
            });
        }

        if ( !usuarioDB ){
            return res.status(400).json({
                ok: false,
                message: 'Usuario o contraseña no validos'
            })
        } 

        if (!bcrypt.compareSync(body.password, usuarioDB.password)){

            return res.status(400).json({
                ok: false,
                message: 'Usuario o contraseña no validos'
            })
        }

        let token= jwt.sign({
            user: usuarioDB,

        },process.env.SEED, {expiresIn: process.env.CADUCIDAD_TOKEN });    

        res.json({
            ok: true,
            user: usuarioDB,
            token
        })
    })

});



module.exports = app;