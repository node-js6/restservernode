const Usuario = require('../models/usuario')
const fs = require('fs');
const path = require('path');
const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();



// default options
app.use(fileUpload());

app.put('/upload/:tipo/:id', function(req, res) {

    let tipo = req.params.tipo;
    let id = req.params.id;

    console.log(tipo);

    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).json({
            ok: false,
            err: 'No files were uploaded.'
        });
      }

    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let archivo = req.files.archivo;
    let nombreArchivo = archivo.name.split('.');
    let extension = nombreArchivo[nombreArchivo.length -1 ];
    
    let extensionesValidas = ['png','jpg','gif','jpeg'];
    let tiposValidos = ['productos','usuarios'];

    
     
    if(tiposValidos.indexOf(tipo) < 0){
        return res.status(400).json({
            ok: false,
            message: 'No es un tipo valido, tipos validos: ' + tiposValidos.join(', '),
            tipo
        })
    }

    console.log('42');
    if (extensionesValidas.indexOf(extension) < 0){
        return res.status(400).json({
                ok: false,
                message: 'las extenciones validas son: ' + extensionesValidas.join(', '),
                ext: extension
        });

    }

    console.log('52');

    let nombreArchivoChg = `${id}-${new Date().getMilliseconds()}.${extension}`
    console.log('nombreArchivoChg: '+nombreArchivoChg);
    archivo.mv(`uploads/${tipo}/${ nombreArchivoChg }`, (err) => {
    if (err){
      return res.status(500).json({
          ok: false,
          err
        });
    };

   imagenUsuario(id, res, nombreArchivoChg,tipo);
//    res.json({
//        ok: true,
//        message: 'cargado'
//    });
  });


});


function imagenUsuario(id, res,nombreArchivoChg,tipo){
    console.log('imagenUsuario')
    console.log('id:' +id);

    Usuario.findById(id, (err, usuarioDb) => {
        console.log(err);
        console.log(usuarioDb);
        if (err){
            borrarArchivo(nombreArchivoChg, tipo)
            return res.status(500).json({
                ok: false,
                err: 'Usuario no encontrado'
            });
        }
        
        if(!usuarioDb){
            borrarArchivo(nombreArchivoChg, tipo)
            return res.status(400).json({
                ok: false,
                message: 'Usuario no existe'
            });
        }

        borrarArchivo(usuarioDb.img, tipo)

        usuarioDb.img = nombreArchivoChg;

        usuarioDb.save((err, usuarioGuardar) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    message: 'Usuario no existe'
                });
            }
            res.json({
                ok: true,
                usuario: usuarioGuardar,
                img: nombreArchivoChg

            })

        });

  });

}


function borrarArchivo(nombreImagen, tipo){

    let pathImage = path.resolve( __dirname, `../../uploads/${tipo}/${nombreImagen}` )
        console.log(pathImage);
        if(fs.existsSync(pathImage)){
            fs.unlinkSync(pathImage)
        }

}

module.exports = app;