const express = require('express')
const bcrypt = require('bcrypt');
const _  = require('underscore');
const bodyParser = require('body-parser');
const Usuario = require('../models/usuario');
const auth = require('../middlewirare/autentication');

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
 
// parse application/json
app.use(bodyParser.json());

app.get('/usuario', auth.verificaToken , (req, res) => {

    let desde = req.query.desde || 0;
    desde = Number(desde);
    let limite = req.query.limite || 5
    limite = Number(limite);
    
    Usuario.find({}, 'nombre email rol estado google img')
           .skip(desde)
           .limit(limite)
           .exec( (err, usuarios) => {
                if (err){
                    return res.status(400).json({
                        ok: false,
                        err
                        })
                }

                Usuario.estimatedDocumentCount({}, (err,conteo) => {
                    res.json({
                        ok: true,
                        usuarios,
                        cuantos: conteo
                    })
                });

           } );
  });
  


  app.post('/usuario', [auth.verificaToken,auth.verifyGranTUser],function (req, res) {
      
      let body = req.body;


      let usuario = new Usuario({
                nombre:   body.nombre,
                email:    body.email,
                password: bcrypt.hashSync(body.password, 10) ,
                rol:      body.rol
      });

      usuario.save( (err, userDB) => {
          if (err){
               return res.status(400).json({
                    ok: false,
                    err
                    })
          }
          
          userDB.password = null;
            res.json({
                   ok: true,
                   user: userDB
            })
          

      });
  
  });
  
  
  app.put('/usuario/:id', [auth.verificaToken,auth.verifyGranTUser],function (req, res) {
    
    let id = req.params.id;
    let body =  _.pick(req.body,['nombre','email','rol','img','estado'] );

    Usuario.findByIdAndUpdate(id,body, {new: true, runValidators: true} ,(err, usuarioDB)=>{
            if (err){
                return res.status(400).json({
                    ok: false,
                    err
                })
            }

            res.json({
                ok: true,
                usuario: usuarioDB
            });
    });

    
  });
  app.delete('/usuario/:id', [auth.verificaToken,auth.verifyGranTUser],function (req, res) {

    let id = req.params.id;
    Usuario.findByIdAndRemove(id, (err,usuarioBorrado) => {
        
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            ok: true,
            usuario: usuarioBorrado
        })
    });
    
  });



module.exports = app;