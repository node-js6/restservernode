
require('./config/config');
const express = require('express')

const app = express();

const mongoose = require('mongoose');
//const bodyParser = require('body-parser')

//configuracion global de rutas
app.use( require('./routes/index'));


// parse application/x-www-form-urlencoded
//app.use(bodyParser.urlencoded({ extended: false }));
 
// parse application/json
//app.use(bodyParser.json());


//mongoose.connect('mongodb://localhost:27017/cafe', {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.connect(process.env.URL_DB,
                   {useNewUrlParser: true, useCreateIndex: true,useUnifiedTopology: true} 
                   , (err, res) => {
    if ( err ){
        throw err;
    }else{
        console.log('Database online');
    }

});

app.listen(process.env.PORT, () => {
    console.log("Escuchando en el puerto ", process.env.PORT);
});
 